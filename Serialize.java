package students;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/**
 * Created by evgenijpopov on 12.06.17.
 */
public class Serialize {

    private static ArrayList<Field> getFields(Object obj) {
        ArrayList<Field> classHierarhyFields  = new ArrayList<>();
        Class cl = obj.getClass();
        while (cl != null) {
            for (Field field : cl.getDeclaredFields()) {
                if ((field.getModifiers() & Modifier.TRANSIENT) != 0) continue;
                if (((field.getModifiers() & Modifier.PRIVATE) != 0) && !cl.equals(obj.getClass())) continue;
                classHierarhyFields.add(field);
            }
            cl = cl.getSuperclass();
        }
        return classHierarhyFields;
    }

    public static boolean toXML(Object obj, String filename) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();
        Document document = impl.createDocument(null,
                null,
                null);

        Element rootElement = document.createElement("Object");
        rootElement.setAttribute("type", obj.getClass().getName());
        document.appendChild(rootElement);
        for (Field f : getFields(obj)) {
            if ((f.getModifiers() & Modifier.TRANSIENT) != 0) continue; // skip this field
            if ((f.getModifiers() & (Modifier.PROTECTED | Modifier.PRIVATE)) != 0) {
                f.setAccessible(true);
            }

            Element e2 = document.createElement("field");
            e2.setAttribute("type", f.getType().toString());
            e2.setAttribute("id", f.getName());
            e2.setAttribute("value", f.get(obj).toString());
            rootElement.appendChild(e2);

            if ((f.getModifiers() & (Modifier.PROTECTED | Modifier.PRIVATE)) != 0) {
                f.setAccessible(false);
            }
        }
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(System.out);
//        StreamResult streamResult = new StreamResult(new File(filename));
        TransformerFactory transFactory = TransformerFactory.newInstance();
        Transformer transformer = transFactory.newTransformer();

        // create the xml file
        //transform the DOM Object to an XML File

        // If you use
        // StreamResult result = new StreamResult(System.out);
        // the output will be pushed to the standard output ...
        // You can use that for debugging
        transformer.transform(domSource, streamResult);
        System.out.println("Done creating XML File");

        return true;
    }
}
