package students.Models;

import java.util.Random;

/**
 * Created by evgenijpopov on 08.06.17.
 */
public class IdGenerate {
    public static Long getId(){
        return System.currentTimeMillis() + new Random().nextLong();
    }
}
