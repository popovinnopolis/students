package students.Models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by evgenijpopov on 08.06.17.
 */
public abstract class Person extends Entity implements Serializable {
    transient private static final long serialVersionUID = -1093674317281948307L;
    protected String firstName;
    protected String surnameName;
    protected String secondName;
    protected Date dateOfBirth;


    protected Person(String firstName, String surnameName, String secondName, Date dateOfBirth) {
        this();
        this.firstName = firstName;
        this.surnameName = surnameName;
        this.secondName = secondName;
        this.dateOfBirth = dateOfBirth;
    }

    protected Person() {
        super();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurnameName() {
        return surnameName;
    }

    public void setSurnameName(String surnameName) {
        this.surnameName = surnameName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String toString() {
        return String.format("%d %s %s %s. %s", id, firstName, surnameName, secondName, dateOfBirth);
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == this) return true;
//        if (obj == null) return false;
//        if (!(obj instanceof Student)) return false;
//        if (!(this.id == ((Student) obj).getId())) return false;
//        return true;
//    }
//
//
//    @Override
//    public int hashCode() {
//        return (int) (21 + id * 42);
//    }
}
