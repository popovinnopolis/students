package students.Models;

/**
 * Created by evgenijpopov on 08.06.17.
 */
public enum ContactType {
    PHONE,
    EMAIL,
    TELEGRAM,
    SKYPE,
    VK,
    FACEBOOK,
    LINKEDIN,
    ADDRESS
}
