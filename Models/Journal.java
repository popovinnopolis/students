package students.Models;

public class Journal extends Entity {
    transient private static final long serialVersionUID = -1288409580042205715L;
    private Student student;
    private Lesson lesson;
    private boolean present; // Присутствовал ли студент?

    public Journal(Student student, Lesson lesson, boolean present) {
        super();
        this.student = student;
        this.lesson = lesson;
        this.present = present;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public boolean isPresent() {
        return present;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Journal journal = (Journal) o;
//
//        if (present != journal.present) return false;
//        if (!student.equals(journal.student)) return false;
//        return lesson.equals(journal.lesson);
//    }
//
//    @Override
//    public int hashCode() {
//        int result = student.hashCode();
//        result = 31 * result + lesson.hashCode();
//        result = 31 * result + (present ? 1 : 0);
//        return result;
//    }
}
