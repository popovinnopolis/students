package students.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Group extends Entity implements Serializable {
    transient private static final long serialVersionUID = -3994677916602304047L;
    private String name;
    private List<Student> students;

    public Group(String name, List<Student> students) {
        super();
        this.name = name;
        this.students = students == null ? new ArrayList<Student>() : students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == this) return true;
//        if (obj == null) return false;
//        if (!(obj instanceof Group)) return false;
//        if (!(this.id == ((Group) obj).getId())) return false;
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        return (int) (21 + id * 42);
//    }
}
