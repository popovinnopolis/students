package students.Models;

import java.io.Serializable;

public abstract class Entity implements Serializable {
    transient private static final long serialVersionUID = -5033508417639375633L;
    protected long id;

    public Entity() {
        this.id = IdGenerate.getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
//        if (obj == null) return false;
//        if (!(obj instanceof T)) return false;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!(this.id == ((IdContainized) obj).getId())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 42);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String toString() {
        return String.format("%d", id);
    }
}
