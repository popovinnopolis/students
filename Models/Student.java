package students.Models;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Student extends Person implements Externalizable {
    transient private static final long serialVersionUID = 1809430313261280407L;
    private Group group;
    private List<Contact> contacts;

    public Student(){
    }

    public Student(String firstName, String surnameName, String secondName, Date dateOfBirth, Group group, List<Contact> contacts) {
        super(firstName, surnameName, secondName, dateOfBirth);
        this.group = group;
        this.contacts = contacts == null ? new ArrayList<Contact>(): contacts;

        List<Student> students = this.group.getStudents();
        students.add(this);
        this.group.setStudents(students);
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public long getGroupId() {
        return group.getId();
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     * The object implements the writeExternal method to save its contents
     * by calling the methods of DataOutput for its primitive values or
     * calling the writeObject method of ObjectOutput for objects, strings,
     * and arrays.
     *
     * @param out the stream to write the object to
     * @throws IOException Includes any I/O exceptions that may occur
     * @serialData Overriding methods should use this tag to describe
     * the data layout of this Externalizable object.
     * List the sequence of element types and, if possible,
     * relate the element to a public/protected field and/or
     * method of this Externalizable class.
     */
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeLong(id);
        out.writeObject(firstName);
        out.writeObject(surnameName);
        out.writeObject(secondName);
        out.writeObject(dateOfBirth);
        out.writeObject(group);
        out.writeObject(contacts);
    }

    /**
     * The object implements the readExternal method to restore its
     * contents by calling the methods of DataInput for primitive
     * types and readObject for objects, strings and arrays.  The
     * readExternal method must read the values in the same sequence
     * and with the same types as were written by writeExternal.
     *
     * @param in the stream to read data from in order to restore the object
     * @throws IOException            if I/O errors occur
     * @throws ClassNotFoundException If the class for an object being
     *                                restored cannot be found.
     */
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
//        id = in.readLong();
        firstName = (String) in.readObject();
        surnameName = (String) in.readObject();
        secondName = (String) in.readObject();
        dateOfBirth = (Date) in.readObject();
        group = (Group) in.readObject();
        contacts = (List<Contact>) in.readObject();
    }
}
