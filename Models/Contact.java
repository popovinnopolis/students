package students.Models;

public class Contact extends Entity  {
    transient private static final long serialVersionUID = -6646732800687978080L;
    private String value;
    private ContactType type;

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == this) return true;
//        if (obj == null) return false;
//        if (!(obj instanceof Contact)) return false;
//        if (!(this.id == ((Contact) obj).getId())) return false;
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        return (int) (21 + id * 42);
//    }

    public Contact(String value, ContactType type) {
        super();
        this.value = value;
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }
}
