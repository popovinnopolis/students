package students;

import students.Models.Group;
import students.Models.Student;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * Created by evgenijpopov on 12.06.17.
 */
public class Main1 {
    public static void main(String[] args) throws IllegalAccessException {
        Group group = new Group("STC_06", null);
        Student student = new Student("John", "Connor", "Reese", new Date(),
                group, null);

        for (Field f: student.getClass().getDeclaredFields()){
            System.out.printf("%s\t\t%s\n", f.getName(), f.getType());
        }

        for (Method m: student.getClass().getDeclaredMethods()){
            System.out.printf("%s\t%s\t%s\n", m.getName(), m.getReturnType(), m.getParameterTypes().length);
        }

        for(Annotation a: Student.class.getAnnotations()){
            System.out.printf("Student %s", a.annotationType());
        }

        Field firstName = null;
        try {
            firstName = student.getClass().getSuperclass().getDeclaredField("firstName");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        firstName.setAccessible(true);

        System.out.printf("%s", firstName.get(student));

        // modificate final id
    }
}
