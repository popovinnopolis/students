package students;

import students.Models.Group;
import students.Models.Student;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Group group = new Group("STC_06", null);
        Student student = new Student("John", "Connor", "Reese", new Date(), group, null);
        Student student1 = new Student("Fred", "Kruger", "Charles", new Date(), group, null);

        System.out.printf("Student: %s \n", student);


        try {
            Serialize.toXML(student, "testXML.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//
//            // create a new file with an ObjectOutputStream
//            FileOutputStream out = new FileOutputStream("test.txt");
//            ObjectOutputStream oout = new ObjectOutputStream(out);
//
//            // write something in the file
//            oout.writeObject(student);
//            oout.writeObject(student1);
//            oout.flush();
//
//            // create an ObjectInputStream for the file we created before
//            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("test.txt"));
//
//            // read and print an object and cast it as string
//            System.out.println("" + ois.readObject());
//            System.out.println("" + ois.readObject());
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }
}
