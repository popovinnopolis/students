package students.Managers;

/**
 * Created by evgenijpopov on 08.06.17.
 */
public interface Crud<T> {
    boolean Create(T item);

    T Read();

    boolean Update(T item);

    boolean Delete(T item);
}
