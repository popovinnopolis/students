package students.Managers;

import students.Models.Student;

/**
 * Created by evgenijpopov on 08.06.17.
 */
public class ManagerStudents implements Crud<Student> {
    @Override
    public boolean Create(Student item) {
        return false;
    }

    @Override
    public Student Read() {
        return null;
    }

    @Override
    public boolean Update(Student item) {
        return false;
    }

    @Override
    public boolean Delete(Student item) {
        return false;
    }
}
