package students.Managers;

import students.Models.Journal;

/**
 * Created by evgenijpopov on 08.06.17.
 */
public class ManagerJournals implements Crud<Journal> {
    @Override
    public boolean Create(Journal item) {
        return false;
    }

    @Override
    public Journal Read() {
        return null;
    }

    @Override
    public boolean Update(Journal item) {
        return false;
    }

    @Override
    public boolean Delete(Journal item) {
        return false;
    }
}
