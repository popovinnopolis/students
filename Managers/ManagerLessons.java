package students.Managers;

import students.Models.Lesson;

/**
 * Created by evgenijpopov on 08.06.17.
 */
public class ManagerLessons implements Crud<Lesson> {
    @Override
    public boolean Create(Lesson item) {
        return false;
    }

    @Override
    public Lesson Read() {
        return null;
    }

    @Override
    public boolean Update(Lesson item) {
        return false;
    }

    @Override
    public boolean Delete(Lesson item) {
        return false;
    }
}
